# All The Files

## **SUPPORT ALL THE FILES!**

![ALL THE THINGS](all-the-things.png)

Credit for All The Things goes to [Hyperbole and a Half](http://hyperboleandahalf.blogspot.com/2010/06/this-is-why-ill-never-be-adult.html)

The goal of this repository is to test a number of different file types to make sure they're supported by GitLab.

## Files

* PDF - `canvas.pdf` from [Mozilla's PDF.js tests](https://github.com/mozilla/pdf.js/blob/master/test/pdfs/canvas.pdf) (Apache 2.0)
* SVG - `gitlab.svg` from GitLab
* STL - `cube.stl` from [Elijah Insua's STL library](https://github.com/tmpvar/stl/blob/master/test/binary/cube.stl) (MIT License)
* CSV - `tarantino.csv` from [fivethirtyeight data repository](https://github.com/fivethirtyeight/data/blob/master/tarantino/tarantino.csv) (MIT License)
* GeoJSON - `dateline.json` from [Mapbox's geojson-vt library](https://github.com/mapbox/geojson-vt/blob/master/test/fixtures/dateline.json) (MIT License)
